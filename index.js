// console.log("h");

// GET ALL TODO LIST USING MAP
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => console.table(json.map((x) => {
	return x.title
})))

// GET SINGLE TODO LIST
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => console.table(`The item ${json[0].title} on the list has a status of ${json[0].completed}`))

// POST
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: "POST",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
			completed: false,
			id: 201,
			title: "Created To Do List Item",
			userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.table(json));

// PUT
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: "PUT",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update my to do list with a different data structure",
			id: 1,
			status: "Pending",
			title: "Updated To Do List Item",
			userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.table(json));

// PATCH
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: "PATCH",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
			dateCompleted: "11/29/2022",
			status: "Complete"
	})
})
.then((response) => response.json())
.then((json) => console.table(json));

// DELETE
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: "DELETE"
});


